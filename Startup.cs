using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Kubernetes;

namespace Gateway
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        public IWebHostEnvironment HostingEnvironment { get; }

        public Startup(IConfiguration configuration,
            IWebHostEnvironment hostingEnvironment)
        {
            this._configuration = configuration;
            this.HostingEnvironment = hostingEnvironment;
        }

        public void ConfigureServices(IServiceCollection services)
        {

            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile("ocelot.json")
                .AddEnvironmentVariables();

            var config = configBuilder.Build();

            services.AddSingleton<IConfiguration>(config);
            services.AddHealthChecks();
            services.AddOcelot(_configuration)
                .AddKubernetes();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHealthChecks("/health");
            app.UseOcelot().Wait();
        }
    }

}
