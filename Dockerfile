FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY Gateway.csproj .
RUN dotnet restore "./Gateway.csproj"
COPY . .
RUN dotnet build "Gateway.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Gateway.csproj" -c Release -o /app

FROM base AS final
# ENV ASPNETCORE_URLS=http://+:5000
WORKDIR /app
COPY --from=publish /app .
RUN groupadd -g 10001 -r app; \ 
    useradd -u 10001  -r -g app app
RUN chmod -R 0755 /app; \
    chown -R app:app /app
USER app
EXPOSE 5000
ENTRYPOINT ["dotnet", "Gateway.dll"]
